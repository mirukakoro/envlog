[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/colourdelete/envlog)

# Envlog

Logger for environment values such as eCO2, TVOC, temp, humidity. Written by myself for a Science Fair assignment in Sep/Oct 2020.
